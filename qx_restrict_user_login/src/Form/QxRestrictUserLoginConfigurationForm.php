<?php

/**
 * @file
 * Contains \Drupal\qx_restrict_user_login\Form\QxRestrictUserLoginConfigurationForm.
 */

namespace Drupal\qx_restrict_user_login\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\system\Entity\Menu;

/**
 * Configure QxRestrictUserLoginConfiguration for this site.
 */
class QxRestrictUserLoginConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'qx_restrict_user_login_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'qx_restrict_user_login.configuration',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('qx_restrict_user_login.configuration');
   
    $form['qx_restrict_user_login'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Restrict Logins to Admins'),
      '#description' => $this->t('Restrict Logins on this platform.'),
      '#default_value' => $config->get('restrict_login'),
    ];


    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    //dsm ("The value on submit is ". $values['qx_restrict_user_login_option']);
    $this->config('qx_restrict_user_login.configuration')
      ->set('restrict_login', $values['qx_restrict_user_login'])
      ->save();
    parent::submitForm($form, $form_state);
  }


}
